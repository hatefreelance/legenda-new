$(window).load(function(){
   $('.fredSel-wrap').carouFredSel({
       auto: {
           play: false
       },
       scroll : {
           items            : 1,
           duration        : 1000,
           pauseOnHover    : true
       },
       pagination: {
           container: '.fredSel-wrap__pagination'
       }
   });

    $('.cases-big-pic').carouFredSel({
        auto: {
            play: false
        },
        height: 550,
        scroll : {
            items            : 1,
            duration        : 500,
            pauseOnHover    : true
        },
        pagination: {
            container: '.fredSel-wrap__pagination'
        }
    });

});

$(document).ready(function(){
   $(document).on('click', '.series__expand', function(e){
       var $parentRow = $(this).closest('.series__row');
       $parentRow.find('.series__items').slideToggle();
       $parentRow.toggleClass('expanded-items');

       if($parentRow.hasClass('expanded-items')){
           $(this).text('Свернуть');
       }else{
           $(this).text('Показать');
       }
   });

    if (typeof $('select').styler === "function") {
        $('select').not('.search-select').styler({
        });

        $('.search-select').styler({
            selectSearch: true,
            selectSearchPlaceholder: 'Поиск...'
        });

        $('input[type=checkbox]').styler();
    }

    //selectSearch

    var $innerPagePic = $('.inner-page-picture');

    if($innerPagePic.length > 0){
        if($innerPagePic.find('img').length > 1){
            $innerPagePic.find('img').css('cursor', 'pointer');
            $innerPagePic.find('img').click(function(e){
                e.preventDefault();
                var index = $(this).index();

                $('html, body').animate({
                    scrollTop: $(".chess-blocks__row").eq(index).offset().top
                }, 1000);
            });
        }
    }
    $('.watch-actions__buy').click(function(e){
        e.preventDefault();

        $('html, body').animate({
            scrollTop: ($('.buy-block').offset().top - 100)
        }, 1000);

    });

    $('.buy-button').not('.cases-buy').click(function(e){
        e.preventDefault();

        $(this).text('В корзине');
        $('.watch-actions__buy').addClass('watch-actions__buy--state_already');

        $('.cart-opener').css('display', 'block');
    });

    $('.watch-actions__fav').click(function(e){
        e.preventDefault();
        $(this).addClass('watch-actions__fav--state_already');
        $('.fav-opener').css('display', 'block');
    });

    $(document).on('click', '.fav-opener', function(e){
        e.preventDefault();
        $('#fav-fixed').animate({
           'right': 0
        }, 600);
    });

    $(document).on('click', '.cart-opener', function(e){
        e.preventDefault();
        $('#cart-fixed').animate({
            'right': 0
        }, 600);
    });

    $(document).on('click', '.fav-block__title', function(e){
        e.preventDefault();
        $(this).closest('.fav-block').animate({
            "right": "-620px"
        }, 600);
    });



    $(document).on('click', '.faq-entry__question', function(e){
        e.preventDefault();
        var $chosenQuestion = $(this).closest('.faq-entry');
        $chosenQuestion.find('.faq-entry__answer').slideToggle();
    });

    /* google maps modals */
    $('.boutique-map-opener').click(function(e){
        e.preventDefault();

        $('#boutique-map').show();
        $(this).closest('.show-room').find('.close-button').show();
        initialize_gmap('boutique-map');
    });

    $('.showroom-map-opener').click(function(e){
        e.preventDefault();

        $('#showroom-map').show();
        $(this).closest('.show-room').find('.close-button').show();
        initialize_gmap('showroom-map');
    });

    $('.close-button').click(function(e){
        e.preventDefault();
        $(this).closest('.show-room__about').find('.google-map').hide();
        $(this).hide();
    });

    /* fancybox inits */

    if (typeof $.fancybox === "function") {
        $('.modal-trigger').fancybox({

            autoSize: 'false',
            fitToView: 'false',
            padding: 70,
            helpers: {
                overlay: {
                    locked: false,
                    css: {
                        'background': 'transparent'
                    }
                }
            },
            afterLoad: function () {
                this.minWidth = $(this.element).data("width");
                this.minHeight = $(this.element).data("height");
            }
        });

        $('.cases-buy').fancybox({
            autoSize: 'false',
            fitToView: 'false',
            padding: 70,
            helpers: {
                overlay: {
                    locked: false,
                    css: {
                        'background': 'transparent'
                    }
                }
            },
            afterLoad: function () {
                this.minWidth = $(this.element).data("width");
                this.minHeight = $(this.element).data("height");
            }
        });

        $('.fancybox-picture').fancybox({
            helpers: {
                overlay: {
                    locked: false,
                    css: {
                        'background': 'rgba(1, 1, 1, 0.9)'
                    }
                }
            },
            padding: 0
        });
    }
});

function initialize_gmap(mapName) {

    // Create an array of styles.
    var styleArray = [
        {
            featureType: "all",
            stylers: [
                { saturation: -80 }
            ]
        },{
            featureType: "road.arterial",
            elementType: "geometry",
            stylers: [
                { hue: "#f1c5a4" },
                { saturation: 50 }
            ]
        },{
            featureType: "poi.business",
            elementType: "labels",
            stylers: [
                { visibility: "off" }
            ]
        }
    ];

    // Create a new StyledMapType object, passing it the array of styles,
    // as well as the name to be displayed on the map type control.
    var styledMap = new google.maps.StyledMapType(styleArray,
        {name: "Styled Map"});

    var centerMap;
    var markerPos;

    if(mapName != 'showroom-map'){
        centerMap = new google.maps.LatLng(43.672261, 40.295011);
        markerPos = new google.maps.LatLng(43.672261, 40.295011);
    }else{
        centerMap = new google.maps.LatLng(55.735253, 37.569287);
        markerPos = new google.maps.LatLng(55.735253, 37.569287);
    }

    var mapOptions = {
        zoom: 17,
        panControl: false,
        mapTypeControl: false,
        center: centerMap,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
    };




    var map = new google.maps.Map(document.getElementById(mapName),
        mapOptions);
    var image = new google.maps.MarkerImage("/img/gmap-marker.png", null, null, null, new google.maps.Size(40,60));
    var marker = new google.maps.Marker({
        position: markerPos,
        map: map,
        icon: image
    });

    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
}
